//desenvolvido por Itamar P. Souza
//e-mail:itasouza@yahoo.com.br

unit untConsultaCepViaCep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, xmldom, XMLIntf, StdCtrls, msxmldom, XMLDoc, Buttons,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,  Mask, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.IB, FireDAC.Phys.IBDef, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.VCLUI.Wait,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TfrmConsultaCepViaCep = class(TForm)
    XMLDocument1: TXMLDocument;
    Label1: TLabel;
    btnOK: TBitBtn;
    SSLIO: TIdSSLIOHandlerSocketOpenSSL;
    editCep: TMaskEdit;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnOKClick(Sender: TObject);


  private
    { Private declarations }

  public
    function ValidaCep(value : String) : String;
  end;

var
  frmConsultaCepViaCep: TfrmConsultaCepViaCep;

  // Dados da cidade
  logradouro : String;
  bairro : String;
  localidade : String;
  uf : String;
  ibge : String;
  gia : String;
  // Dados da cidade

implementation

{$R *.dfm}

{ TForm1 }



function TfrmConsultaCepViaCep.ValidaCep(value: String): String;
var
  tempXML :IXMLNode;
  tempNodePAI :IXMLNode;
  tempNodeFilho :IXMLNode;
  I :Integer;
begin
   XMLDocument1.FileName := 'https://viacep.com.br/ws/'+Trim(editCep.Text)+ '/xml/';
   XMLDocument1.Active := true;
   tempXML := XMLDocument1.DocumentElement;

   tempNodePAI := tempXML.ChildNodes.FindNode('logradouro');

   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      logradouro := tempNodeFilho.Text;
   end;

   tempNodePAI := tempXML.ChildNodes.FindNode('bairro');
   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      bairro := tempNodeFilho.Text;

   end;


  tempNodePAI := tempXML.ChildNodes.FindNode('localidade');
   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      localidade :=  tempNodeFilho.Text;
   end;


  tempNodePAI := tempXML.ChildNodes.FindNode('uf');
   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      uf := tempNodeFilho.Text;
   end;


   tempNodePAI := tempXML.ChildNodes.FindNode('ibge');
   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      ibge := tempNodeFilho.Text;
   end;


   tempNodePAI := tempXML.ChildNodes.FindNode('gia');
   for i := 0 to tempNodePAI.ChildNodes.Count - 1 do
   begin
      tempNodeFilho := tempNodePAI.ChildNodes[i];
      gia := tempNodeFilho.Text;
   end;

end;
end.
